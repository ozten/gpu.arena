# Art Direction

Mood board: Tron, Neon, 80's, Beatsaber, Synth, Hockey Arena

## Audio

* https://freesound.org/people/RokZRooM/sounds/508004/
* https://freesound.org/people/Inavision/sounds/479258/
* https://freesound.org/people/room/sounds/7444/    
* https://freesound.org/people/bahgheera/sounds/13161/
* https://freesound.org/people/Xinematix/sounds/262442/

### GPU Player loading

* https://freesound.org/people/stratcat322/sounds/171239/

### Menu Audio

* https://freesound.org/people/kiddpark/sounds/457211/

### Menu UI Audio
* https://freesound.org/people/qubodup/sounds/213536/

## Models

### Spectator

paper boxes?!?
https://bigblueboo.tumblr.com/post/122122871774/the-chomps
Googly Eyes would be fun, not very Cyberpunk...


Fog stadium?
https://bigblueboo.tumblr.com/post/103983762816

https://free3d.com/3d-model/lowpoly-male-full-riged-blender-file-85929.html

https://www.blendswap.com/blend/11322

https://www.blendswap.com/blend/20326

https://www.blendswap.com/blend/18377
https://www.blendswap.com/blend/25635