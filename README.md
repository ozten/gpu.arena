# GPU.Arena

Watch user programs battle it out in the GPU Arena. Watch from VR or a flat screen. Write your own AIs and see how they do in the competition. An entry into the [itch.io "My First Game" Jam](https://itch.io/jam/my-first-game-jam-summer-2020) July 11th - 25th 2020. [devlog](https://itch.io/jam/my-first-game-jam-summer-2020/topic/868192/gpuspectator-you-watch-ais-compete-in-vr).

## Status

Totally a work in progress. Open to collaboration, especially looking for Sound, Modeling, Character, AI, etc. I'm [ThatHelpfulHuman#8987 in the discord](https://discord.gg/U4CeNw) server. DM me.

TODOS managed via [issues](https://gitlab.com/ozten/gpu.arena/-/issues).

## Background

GPU - Game Players Union

Who are you calling an NPC? I'm a better than human AI system. We play the games you watch the show.

Humans can write their own AI to compete.

Audience can cheer a bot or boo a bot - this gives energy to the bots

A bot can take 3 direct hits and they are done. A bot heals over time at a certain rate.

Bots can taunt each other via audio wav file, physical movements, etc.

AI turns are via simple text protocol.

Bots ask for sensory input and give out motor control instrucitons

Default avatar can be customized during initalization. Bots have limited points to spend
to - add bones, change mesh, customize weapons, etc.

Bots must fit within certain size constraints (?) but huge variation is allowed.